Betű Szerkesztő
===============

<imgt ../img/font.png> Kattints a betű ikonra (vagy <kbd>F5</kbd>) a betűkészlet módosításához.

Az itt szerkesztett betűtípust fogja használni, amikor a [width] megméri a szöveg méretét, és amikor a [text] paranccsal kiírsz.

<imgc ../img/fontscr.png><fig>Betű Szerkesztő</fig>

Ennek az oldalnak az elrendezése hasonló, mint a [szprájt szerkesztő], csak nincs paletta. Balra található a glifszerkesztési
terület (<ui2>1</ui2>), jobbra pedig a glifválasztó (<ui2>2</ui2>). (A glif egy adott betű, azaz UNICODE kódpont megjelenített
alakja).

Glifszerkesztő
--------------

Elég egyszerű, <mbl> balklikk beállítja a kinézetet (előtér), a <mbr> pedig törli (háttér).

Ha lenyomjuk a <kbd>Shift</kbd>-et, akkor vonalat húzhatunk a legutóbb módosított pontból.

Glifválasztó
------------

Kereshetsz UNICODE kódpontra, de ha csak leütsz egy billentyűt, akkor a glifválasztó a glifjére fog ugrani. Ha a billentyűzeten
nincs meg valamelyik gomb, akkor a szokásos beviteli módok is használhatók, lásd [billentyűzet](#ui_kbd).

<h2 font_tools>Eszköztár</h2>

Az elérhető eszközök itt szűkösebbek, eltolás, forgatás és tükrözés van csak, nincsen kijelölés. De a glifválasztóban a
kimásolás (<kbd>Ctrl</kbd>+<kbd>C</kbd>) és a beillesztés (<kbd>Ctrl</kbd>+<kbd>V</kbd>) a megszokott módon működik.
